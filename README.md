# POP Kiosk

The POP kiosk was built with open hardware and open-source software technologies. The prototype is designed to have a mobile and modular form factor, allowing it to be integrated into a variety of pop-up exhibit scenarios.

# Installation
Download the appropriate zip file and extract it.  Burn the extracted image (.img) file to a 32gb or larger SD card using [BalenaEtcher](https://www.balena.io/etcher/) or the [Raspberry Pi Imager](https://www.raspberrypi.com/software/).
Installation guides can be found on our wiki [here](https://cliomuseums.org/wiki/).

# Usage
Once the image has been burned to an SD card, it can be placed in the Raspberry Pi and powered on.

# Contributing
Bug reports and feature requests are welcome.

# License
MIT
